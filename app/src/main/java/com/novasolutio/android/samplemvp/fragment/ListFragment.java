package com.novasolutio.android.samplemvp.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.novasolutio.android.samplemvp.R;
import com.novasolutio.android.samplemvp.adapter.NoteListAdapter;
import com.novasolutio.android.samplemvp.entity.Note;
import com.novasolutio.android.samplemvp.presenter.ListFragmentPresenter;
import com.novasolutio.android.samplemvp.view.IListFragmentView;

import java.util.ArrayList;

public class ListFragment extends Fragment implements IListFragmentView {

    public static final String TAG = "ListFragment";

    private static ListFragment instance;

    private NoteListAdapter mAdapter;

    private ListFragmentPresenter mPresenter;

    public static ListFragment getInstance() {
        if (instance == null) {
            instance = new ListFragment();
        }

        return instance;
    }

    public ListFragment() {
        //да, надо дагером либо средствами мокси инжектить, но я для простоты просто создаю экземпляр
        mPresenter = new ListFragmentPresenter();
        mPresenter.bindView(this);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list, container, false);
        initUI(view);
        mPresenter.onViewCreated();
        return view;
    }

    private void initUI(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.rv_list);
        mAdapter = new NoteListAdapter(getContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void showList(ArrayList<Note> notes) {
        mAdapter.addData(notes);
    }
}
