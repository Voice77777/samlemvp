package com.novasolutio.android.samplemvp.repository;

import com.novasolutio.android.samplemvp.entity.Note;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Repository implements IRepository {

    private static Repository instance;

    private ArrayList<Note> mNotes = new ArrayList<>();

    public static Repository getInstance() {
        if (instance == null) {
            instance = new Repository();
        }

        return instance;
    }

    public Repository() {
        mockNotes();
    }

    @Override
    public List<Note> getAllNotes() {
        return mNotes;
    }

    @Override
    public void addNote(Note note) {
        mNotes.add(note);
    }

    //Метод наполняет массив заранее созданными записями. Симуляция работы с базой данных
    private void mockNotes() {
        mNotes.add(new Note(0, "Some note 0", new Date().getTime()));
        mNotes.add(new Note(1, "Some note 1", new Date().getTime()));
        mNotes.add(new Note(2, "Some note 2", new Date().getTime()));
    }


}
