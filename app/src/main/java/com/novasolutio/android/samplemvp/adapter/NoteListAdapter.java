package com.novasolutio.android.samplemvp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.novasolutio.android.samplemvp.R;
import com.novasolutio.android.samplemvp.entity.Note;
import com.novasolutio.android.samplemvp.utill.NoteDiffUtillCallback;

import java.util.ArrayList;
import java.util.List;

import static com.novasolutio.android.samplemvp.fragment.ListFragment.TAG;

public class NoteListAdapter extends RecyclerView.Adapter<NoteListHolder> {

    private Context mContext;
    private ArrayList<Note> mNotes;

    public NoteListAdapter(Context context) {
        mContext = context;
        mNotes = new ArrayList<>();
    }

    public void addData(ArrayList<Note> notes) {
        mNotes = notes;
        updateList();
        //notifyDataSetChanged();   этот метод не нужен
    }

    public void updateList() {
        List<Note> newList = new ArrayList<>();
        DiffUtil.DiffResult diffResult =
                DiffUtil.calculateDiff(new NoteDiffUtillCallback(this.mNotes, newList));
        diffResult.dispatchUpdatesTo(this);
    }

    private Note getNoteItem(int position) {
        return mNotes.get(position);
    }

    @NonNull
    @Override
    public NoteListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item, viewGroup, false);

        return new NoteListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteListHolder noteListHolder, int i) {
        //Отображение отрисовки позиции
        Log.d(TAG, "bind, item_pos" + i);
        noteListHolder.bind(getNoteItem(i));
    }

    @Override
    public int getItemCount() {
        return mNotes.size();
    }
}
