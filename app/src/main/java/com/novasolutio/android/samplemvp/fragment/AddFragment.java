package com.novasolutio.android.samplemvp.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.novasolutio.android.samplemvp.R;
import com.novasolutio.android.samplemvp.presenter.AddFragmentPresenter;
import com.novasolutio.android.samplemvp.view.IAddFragmentView;

public class AddFragment extends Fragment implements IAddFragmentView {
    public static final String TAG = "AddFragment";

    private static AddFragment instance;

    private EditText mInput;
    private Button mButton;

    private AddFragmentPresenter mPresenter;

    public static AddFragment getInstance() {
        if (instance == null) {
            instance = new AddFragment();
        }

        return instance;
    }

    public AddFragment() {
        mPresenter = new AddFragmentPresenter();
        mPresenter.bindView(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.add_fragment, container, false);
        initUI(view);

        return view;
    }

    private void initUI(View view) {
        mInput = view.findViewById(R.id.et_input);
        mButton = view.findViewById(R.id.btn_add);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.onAddBtnClicked(mInput.getText().toString());
            }
        });
    }

    @Override
    public void clearInput() {
        mInput.setText("");
    }

    @Override
    public void showError(String errorMsg) {
        Toast.makeText(getContext(), errorMsg, Toast.LENGTH_SHORT).show();
    }
}
