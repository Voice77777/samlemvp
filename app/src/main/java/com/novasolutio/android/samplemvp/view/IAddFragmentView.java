package com.novasolutio.android.samplemvp.view;

public interface IAddFragmentView {
    void clearInput();
    void showError(String errorMsg);
}
