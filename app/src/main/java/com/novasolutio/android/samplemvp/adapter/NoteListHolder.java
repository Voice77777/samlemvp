package com.novasolutio.android.samplemvp.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.novasolutio.android.samplemvp.R;
import com.novasolutio.android.samplemvp.entity.Note;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class NoteListHolder extends RecyclerView.ViewHolder {

    private TextView mNoteText, mNoteDate;

    public NoteListHolder(@NonNull View itemView) {
        super(itemView);

        initUI(itemView);
    }

    private void initUI(View itemView) {
        mNoteText = itemView.findViewById(R.id.tv_noteText);
        mNoteDate = itemView.findViewById(R.id.tv_noteDate);
    }

    public void bind(Note note) {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        mNoteText.setText(note.getNoteText());
        mNoteDate.setText(dateFormat.format(note.getDate()));
    }


}
